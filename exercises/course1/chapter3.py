# # Ex 1
# # From matplotlib, import pyplot under the alias plt
# from matplotlib import pyplot as plt

# # Plot Officer Deshaun's hours_worked vs. day_of_week
# plt.plot(deshaun.day_of_week, deshaun.hours_worked)

# # Display Deshaun's plot
# plt.show()


# # Plot Officer Deshaun's hours_worked vs. day_of_week
# plt.plot(deshaun.day_of_week, deshaun.hours_worked)

# # Plot Officer Aditya's hours_worked vs. day_of_week
# plt.plot(aditya.day_of_week, aditya.hours_worked)

# # Plot Officer Mengfei's hours_worked vs. day_of_week
# plt.plot(mengfei.day_of_week, mengfei.hours_worked)

# # Display all three line plots
# plt.show()


# # orange


# # Ex 2
# # Officer Deshaun
# plt.plot(deshaun.day_of_week, deshaun.hours_worked, label='Deshaun')

# # Add a label to Aditya's plot
# plt.plot(aditya.day_of_week, aditya.hours_worked, label='Aditya')

# # Add a label to Mengfei's plot
# plt.plot(mengfei.day_of_week, mengfei.hours_worked, label='Mengfei')

# # Add a command to make the legend display
# plt.legend()

# # Display plot
# plt.show()


# # Mengfei


# # Lines
# plt.plot(deshaun.day_of_week, deshaun.hours_worked, label='Deshaun')
# plt.plot(aditya.day_of_week, aditya.hours_worked, label='Aditya')
# plt.plot(mengfei.day_of_week, mengfei.hours_worked, label='Mengfei')

# # Add a title
# plt.title('hours worked vs day of week')

# # Add y-axis label
# plt.ylabel('hours worked')

# # Legend
# plt.legend()
# # Display plot
# plt.show()


# # Create plot
# plt.plot(six_months.month, six_months.hours_worked)

# # Add annotation "Missing June data" at (2.5, 80)
# plt.text(2.5, 80, "Missing June data")

# # Display graph
# plt.show()


# # Ex 3
# # Change the color of Phoenix to `"DarkCyan"`
# plt.plot(data["Year"], data["Phoenix Police Dept"], label="Phoenix", color='DarkCyan')

# # Make the Los Angeles line dotted
# plt.plot(data["Year"], data["Los Angeles Police Dept"], label="Los Angeles", linestyle=':')

# # Add square markers to Philedelphia
# plt.plot(data["Year"], data["Philadelphia Police Dept"], label="Philadelphia", marker='s')

# # Add a legend
# plt.legend()

# # Display the plot
# plt.show()


# # Change the style to fivethirtyeight
# plt.style.use('fivethirtyeight')

# # Plot lines
# plt.plot(data["Year"], data["Phoenix Police Dept"], label="Phoenix")
# plt.plot(data["Year"], data["Los Angeles Police Dept"], label="Los Angeles")
# plt.plot(data["Year"], data["Philadelphia Police Dept"], label="Philadelphia")

# # Add a legend
# plt.legend()

# # Display the plot
# plt.show()


# # Change the style to ggplot
# plt.style.use('ggplot')

# # Plot lines
# plt.plot(data["Year"], data["Phoenix Police Dept"], label="Phoenix")
# plt.plot(data["Year"], data["Los Angeles Police Dept"], label="Los Angeles")
# plt.plot(data["Year"], data["Philadelphia Police Dept"], label="Philadelphia")

# # Add a legend
# plt.legend()

# # Display the plot
# plt.show()


# # Choose any of the styles
# plt.style.use('seaborn')

# # Plot lines
# plt.plot(data["Year"], data["Phoenix Police Dept"], label="Phoenix")
# plt.plot(data["Year"], data["Los Angeles Police Dept"], label="Los Angeles")
# plt.plot(data["Year"], data["Philadelphia Police Dept"], label="Philadelphia")

# # Add a legend
# plt.legend()

# # Display the plot
# plt.show()


# # Plot each line
# plt.plot(ransom.letter, ransom.frequency,
#          label='Ransom', linestyle=':', color='gray')
# plt.plot(suspect1.letter, suspect1.frequency, label='Fred Frequentist')
# plt.plot(suspect2.letter, suspect2.frequency, label='Gertrude Cox')

# # Add x- and y-labels
# plt.xlabel("Letter")
# plt.ylabel("Frequency")

# # Add a legend
# plt.legend()

# # Display plot
# plt.show()