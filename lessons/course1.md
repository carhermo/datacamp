# Introduction to Data Science in Python

## _Chapter 1_: Getting Started in Python

1. Dive into Python

Welcome to this course where you will begin your journey through data science! My name is Hillary Green-Lerman, and I'll be your guide to the wonderful world of Python. The purpose of this course is to gently introduce you to programming and data science by playing with some simple datasets. This course is intended for learners with no coding experience. I'll aim to answer all of your questions as we move through the course.

2. What you'll learn

In this course, you'll learn: how to write and execute Python code with DataCamp, load data from a spreadsheet file into Python, and turn that loaded data into beautiful plots. By the end of this course, you'll be familiar with Python syntax and ready to learn more.

3. Solving a mystery with data

While we learn, we'll be solving a mystery using data. Someone has kidnapped Bayes, DataCamp's prize-winning Golden Retriever. The kidnapper has left clues that we can analyze. We'll use techniques like chemical analysis and letter frequency to pick the correct suspect.

4. Using the IPython shell

Before we can solve our mystery, we need to get familiar with how code is written. There are two ways of executing code on DataCamp. The first is the IPython Shell, located in the bottom-right of each exercise. In the Python Shell (which is sometimes called "the console"), we can type a single line of code use the "Return" key to execute that line. This is a good place for experimenting with new ideas.

5. Using the script editor

The second place we can enter code is the script editor, located in the top-right of each exercise. The script editor lets us write multiple lines of code, as well as comments, which are lines beginning with a "pound" or "hash" symbol. When we are ready to execute all of the code in the script editor, we can click "Run Code". When we think our code is correct, we can click "Submit Answer".

6. What is a module?

Now that we know where to write code, let's dive into our first concept: modules. Modules help group together related tools in Python. For example, we might want to group together all of the tools that make different types of charts: bar charts, line charts, and histograms. Some common examples of modules are matplotlib (which creates charts), pandas (which loads tabular data), scikit-learn (which performs machine learning), scipy (which contains statistics functions), and nltk (which works with text data).

7. Importing pandas and matplotlib

We must import any modules that we plan on using before we can write any other code. We do this at the top of the script editor. If we don't import modules, we can't use the tools that they contain. In this example, by importing the modules pandas and matplotlib, we're able to unbox the tools necessary to create a graph. In this case, pandas gives us the tools to read data from a file, and matplotlib gives us the tools to plot the data.

8. Importing a module

To import a module, simply type "import" followed by a space and then the module name. Oftentimes, module names are long, so we can shorten them by using an alias. To give your module an alias, just add "as" and a shorter name to your original import statement. This statement will alias "pandas" as "pd".

9. Let's practice!

Let's practice what we've learned using the console and script editor!


1. Creating variables

Previously, you started writing Python code in the script editor and the console. You learned what a module is, and how to import it. You also learned to simplify module names using an alias. In this lesson, you'll learn about variables, which help us reference a piece of data for later use.

2. Filing a missing puppy report

In the previous lesson, we told you about our kidnapped Golden Retriever, Bayes. To solve the mystery, let's start by filling out a Missing Puppy Report. In order to file the report, we'll need to record some information about Bayes, such as his height and weight. In Python, we will represent each line from the missing puppy report with a variable. A variable gives us an easy-to-use shortcut to a piece of data. Whenever we use the variable name in our code, it will be replaced with the original piece of data. In this case, one of our variables is "name" and its value is "Bayes". Another variable is "height" and its value is "24". We define variables using an equals sign.

3. Rules for variable names

When defining variables, we need to follow a few rules. Variables must start with a letter. You can use a capital letter, but we usually use lowercase. After the first letter, we can use letters, numbers, and underscores in our variable name. We can't use special characters like exclamation points or dashes. Variable names are case sensitive, so these two different ways of typing "my_var" would be different variables. On the left are some examples of valid variable names, and on the right are some examples of invalid variable names.

4. Error messages

Let's see what happens when we try to use an invalid variable name. The variable bayes-height is invalid because of the hyphen. When we try to enter it, we will receive a SyntaxError. Above the Syntax Error, we see the line of code that caused the problem and a caret that indicates approximately where the problem occurred.

5. Floats and strings

Variables come in many "flavors". Two important flavors are floats and strings. Floats represent either integers or decimals. Strings represent text and can contain letters, numbers, spaces, and special characters. We define a string by putting either single or double quotes around a piece of text.

6. Common string mistakes

It's easy to get errors when working with strings. If you get one, there are two likely causes. You might have forgotten to put quotation marks around your string. If you do this, Python will think that your string is a variable, and if that variable wasn't previously defined, you'll get a "name error". Alternatively, you might have started your string with one type of quote and finished with a different type. If you mix single and double quotes, you'll get a syntax error.

7. Displaying variables

If we want to know the current value of one of our variables, we can use "print". We simply type the word "print" and put our variable name inside of the parentheses. When we execute the code, the value will appear in the console. Remember: the variable name is not a string, so we don't put it in quotes.

8. Let's practice!

Now that you know how to define and display variables, let's practice!


1. What is a function?

Previously, we learned about variables (which allow us to store information for later use), strings (which represent text), and floats (which represent numbers). Now that we know the basic data types in Python, we'll start using them with functions.

2. A function is an action

A function is an action. It turns one or more inputs into an output. In this example, the function is called "turn_orange". The input is a shape (such as a blue square) and the "action" is to color the input shape orange.

3. Functions in code

Consider the following code snippet. In a previous lesson, we learned that this code would read some data and produce a graph. In this snippet there are three functions: pd-dot-read_csv turns a csv file into a table in Python, plt-dot-plot turns data into a line plot, and plt-dot-show displays the plot in a new window. Let's learn about functions by examining one from our code snippets.

4. Anatomy of a function

This function takes the data from the table in the bottom-left and plots letter_index on the x-axis and frequency on the y-axis, resulting in the graph on the bottom-right.

5. Anatomy of a function: function name

First, let’s look at the function name. The function name has two parts: the first tells us what module the function comes from. In this case, it's from plt, which was the alias we used when we imported matplotlib. The second part (which comes after the period) is the name of the function: plot. After the name of the function, comes a set of parentheses. The inputs to the function will all come inside of these parentheses.

6. Anatomy of a function: positional arguments

Positional arguments are one type of input that a function can have. Positional arguments must come in a specific order. In this case, the first argument is the x-value of each point, and the second argument is the y-value of each point. Each argument is separated by a comma. If you forget the comma, you will get a syntax error. It's good practice to put a space after the comma, but your code will run even if you forget that space. Keyword arguments come after

7. Anatomy of a function: keyword arguments

positional arguments, but if there are multiple keyword arguments, they can come in any order. In this case, the keyword argument is called "label". After the equals sign, we've put the actual input to the function, which is "Ransom". Eventually, this argument will let us create a legend for our graph. Before you start practicing, let's discuss

8. Common function errors

two common errors for functions: If you get a syntax error, you might be missing commas between each argument. Both positional and keyword arguments need to be separated by commas. Alternatively, you might be missing a parenthesis at the end of your function. While you write code, syntax highlighting in the script editor will help remind you to close your parentheses.

9. Let's practice!

Let's practice what you've learned by making a few graphs!

## _Chapter 2_: Loading Data in Pandas

1. What is pandas?

In the previous chapter, you learned about basic Python, including defining variables and loading modules. One of the modules that you loaded was called pandas. Pandas is a module for working with tabular data, or data that has rows and columns. Common examples of tabular data are spreadsheets or database tables. In this chapter, you'll learn the basics of Pandas, including how to load and inspect data.

2. What can pandas do for you?

Pandas gives you many tools for working with tabular data. You can load tabular data from multiple sources (like spreadsheets or databases), search for particular rows or columns in your loaded data, calculate aggregate statistics (like averages or standard deviations), and combine data from multiple sources.

3. Tabular data with pandas

You already know two data types: floats and strings. Pandas introduces a new, more powerful data type: the DataFrame, which represents tabular data. Loading data into a DataFrame is the first step in using Pandas.

4. CSV files

One of the easiest ways to create a DataFrame is by using a CSV file. CSV stands for comma-separated values, and is a common way of storing tabular data as a text-only file. You can download a CSV version of data from an Excel spreadsheet, a SQL database, or a Google Sheet.

5. Loading a CSV

Before we can start using Pandas, we have to import the pandas module. Recall that we always import Pandas under the alias "pd". Next, we create our first DataFrame from a CSV. Turning a CSV into a DataFrame is easy. We use a function: pd-dot-read_csv. This function takes one argument, the name of a CSV file as a string. In this example, the name of the file is ransom-dot-csv.

6. Displaying a DataFrame

Notice that we saved this DataFrame to a variable called "df". We can display this variable by using the "print" function, which we learned about in a previous lesson. When we print a DataFrame, we get to see every row in the DataFrame. In this example, the DataFrame is so large that it doesn't entirely fit on the slide!

7. Inspecting a DataFrame

Usually, we don't want to print an entire DataFrame to inspect it; we just want to view the first few lines. We can do this by using head. head behaves slightly differently than the functions we've learned about previously. Rather than coming at the beginning of a line of code, this function "belongs" to the DataFrame variable and comes at the end of the line, after a period. We call this type of function a "method". The .head method just selects the first five rows of "df". In order to display then, we need to put df-dot-head inside of the print function. Printing the first few lines of a DataFrame using .head is much easier to read than printing the entire DataFrame.

8. Inspecting a DataFrame

Another way of learning about a DataFrame is using the info method. Like head, we type the variable name of the DataFrame (in this case "df"), followed by a period, and then followed by our method dot-info. Again, in order to display the output, we put the whole statement inside of a print function. Let's take a closer look at the output of info.

9. Inspecting a DataFrame

Notice that we can see the names of the columns, the number of rows, and data types for each column. This method is particularly useful for DataFrames with many columns that are difficult to display using head.

10. Let's practice!

Now that you know what a DataFrame is, how to load data with a CSV, and how to inspect a DataFrame, let's continue to solve the mystery of Bayes' kidnapping.


1. Selecting columns

Now that we've loaded data from a CSV into a DataFrame, we're going to want to access that data. The first way we'll use that data is by selecting columns. This means we'll be getting all values from a particular column in a DataFrame. In this lesson, we'll continue working with the credit card reports for our suspects in the kidnapping of Bayes, the Golden Retriever, as well as a few other pieces of evidence from the case.

2. Why select columns?

Why do we want to select columns? We might use them in a calculation. For example, this code selects the column "price" and then calls the method "sum" on that column to get the total amount of money spent by our suspects. We might also want to use the data as the input to a function. You might recognize this code from when we learned about functions. It will create a plot of the frequencies of each letter in the ransom note. The data comes from a DataFrame called "ransom" with columns "letter" and "frequency".

3. Columns names are strings

So how do we select columns? We know that we can view the column names for our data using either dot-head or dot-info. If we examine the credit_records DataFrame, we see that there are 5 columns: suspect, location, date, item, and price. Each of these column names is a string. Recall that a string is just a way of representing text in Python. In this case the strings are all single words, but strings can have spaces or special characters. That means that columns in DataFrames can also have spaces or special characters.

4. Selecting with brackets and string

We can use these column name strings to select a column. We type the name of the DataFrame, followed by an open square bracket, followed by the column string (in quotation marks), and closed with a second square bracket. If we print the column, we see all values for that particular column.

5. Selecting with a dot

There's a second way we can select columns from a DataFrame. If the columns string only contains letters, numbers, and underscores, we can use dot notation. For dot notation, we simply type the name of the variable, followed by a dot, followed by the name of the column. In this case, we don't use quotation marks around the column name.

6. Common mistakes in column selection

Let's go over some common errors you might encounter when selecting columns. Remember that when your column name contains spaces or special characters, you need to use bracket and string notation, and not dot notation to select the column. If you use dot notation, Python gets confused and thinks that you're referring to several different variables.

7. Common mistakes in column selection

Another common error is forgetting to put quotation marks around the column string when using brackets and string notation. If you forget the quotation marks, then Python thinks that the column name is actually a variable that hasn't been defined yet.

8. Common mistakes in column selection

Finally, remember that square brackets are not the same at parentheses. If you use parentheses, Python will think that you're using the DataFrame as a function, and will give a "TypeError".

9. Let's practice!

You've learned how to select columns from a DataFrame and you're ready to deal with any errors you might encounter. Let's practice!


1. Select rows with logic

Welcome back. Now that we know how to select columns of a DataFrame, let's learn how to select specific rows.

2. Continuing the investigation

We've collected credit card statements from all of our suspects into a DataFrame called "credit_records". If we can prove that some of these suspects made suspicious purchases, we can ask them for writing samples and match those samples to the ransom note found at the scene of the crime. In order to search for the suspicious purchases, we'll need to select rows from the DataFrame using logical statements.

3. Logical statements in Python

In Python, a logical statement checks for a relationship between two values (such as "equal to" or "greater than"), and returns True or False. For example, suppose we want to know if 12 times 8 is equal to 96. We can compare the variables "question" and "solution" using a double equals sign. This tests whether the two numbers are the same. In this case, Python returns "True" because 12 times 8 is equal to 96. That "True" that Python returns is a special data type called a "Boolean". There are only two Booleans: True and False. Recall that you've already learned three other data types: floats, strings, and DataFrames. We'll be using Booleans to select rows where the value of a column matches a specific value.

4. Other types of logic

We can make many types of comparisons in Python, such as greater than, greater than or equal to, less than, and less than or equal to. In this example, we ask if the variable "price" is greater than 5. Python returns False because 2-point-25 is not greater than 5. We can also check if two variables are not equal to each other. We do this using exclamation point followed by an equals sign. In this example, we see that lowercase "bayes" is not the same as title-case "Bayes".
5. Using logic with DataFrames

In the previous examples, we were just comparing two values. In a DataFrame we can compare one value to all values in a DataFrame. For example, we can compare if each purchase in our credit cared records had a price that was greater than \$20. This returns an entire column of True or False.

6. Using logic with DataFrames

If we put that truth testing statement inside of brackets, we can select only the rows where the statement is True. Here, we select rows of credit_records where the price is greater than 20 dollars.

7. Using logic with DataFrames

Let's examine this line of code more closely. We start with the name of the DataFrame we want to select rows from. In this case, "credit_records". Next we have a set of square brackets. Inside of the square brackets we put our logical test. In this case, the logical test is whether the "price" column of "credit records" is greater than \$20. This statement will select all rows of credit_records where the column price is greater than \$20.

8. Using logic with DataFrames

Let's try another example. Suppose, we want to select all rows of credit_records where the "suspect" is equal to "Ronald Aylmer Fisher". Again, we have the name of the DataFrame, followed by square brackets. Inside of the brackets, we check if credit_records-dot-suspect is equal to "Ronald Aylmer Fisher". Note that we use the double equals sign to test equality.

9. Let's practice!

Great job! Let's practice.


## _Chapter 3_: Plotting Data with matplotlib

1. Creating line plots

In this video, we'll learn how to create line plots using another Python module: matplotlib. You've seen matplotlib before in Chapter 1, when you learned about the function plt-dot-plot. Recall that plt is the alias that we use when importing matplotlib.

2. The plot thickens

We're close to solving the mystery of Bayes, the kidnapped golden retriever. Remember that a ransom note was found at the scene of the crime. In the previous chapter, we narrowed the list of suspects to Freddie Frequentist and Gertrude Cox. We've asked Freddie and Gertrude to give us writing samples to compare against the ransom note. We'll compare the letter frequencies in Freddie and Gertrude's samples to the note found at the scene of the crime. We'll do this by creating line plots.

3. From DataFrame to Visualization

We've stored the letter frequencies in a Pandas DataFrame. We'll need to transform that DataFrame into a two-dimensional line plot. A line plot uses a coordinate grid to plot a series of points and then connects each point using a line. In this case, the x-axis represents the letters of the alphabet and the y-axis represents the frequency of each letter.

4. Introducing Matplotlib

In order to create a line plot, we'll need to import the module matplotlib. Generally, Data Scientists only load in the submodule pyplot and use the alias plt. We do this by typing "from matplotlib import pyplot as plt". Once we've loaded our module, we can use the functions plt-dot-plot and plt-dot-show to create and display our line plot.

5. Line Plot

Let's examine the line plot function. We begin with the name of the function: plt-dot-plot Like all functions, we'll be putting the arguments inside of parentheses. The first argument is the x-values we want to plot. In this case, we use the letter column from the DataFrame ransom. Recall that we select a column by typing the name of the DataFrame, followed by a dot, followed by the column name. The second argument is the y-values we want to plot. In this case, we use the frequency column from the DataFrame ransom. Again, we type DataFrame name, then dot, then column name. Finally, we close the parentheses.

6. Displaying the Results

If we only type plt-dot-plot, nothing will show up. That's because Python wants to give us the opportunity to enter other commands. For example, we might want to add another line or a title for the plot. When we want to display everything that we've made, we use a second function: plt-dot-show. This function takes no arguments. You just type it, and then your plot will pop open in a new window.

7. Multiple Lines

If we want to plot multiple lines on the same axis, we can just add a second plt-dot-plot command before adding plt-dot-show. In this example, we plotted two different datasets: data1, which represents a straight line and data2, which represents a parabola. Matplotlib will automatically give the two lines different colors.

8. Let's practice!

Let's practice making line plots!


1. Adding labels and legends

In the previous video, we learned how to create a line plot using plt-dot-plot and how to display the plot using plt-dot-show. In this lesson, we'll explain our plot to potential viewers by adding labels and legends.

2. What did we just plot?

Previously, we plotted the frequency of each letter in the ransom note found at the scene of the kidnapping of Bayes. However, if we showed this plot to a judge without any context, she wouldn't know what we were trying to communicate. When we share our graphs with an audience, we must add labels to make it clear what we've plotted and why it's important.

3. Axes and title labels

First, let's add some axis labels. We can label the horizontal (or "x") axis using the command plt-dot-xlabel. This function takes one argument: a string that represents what we want the label to read. Similarly, we can label the vertical (or "y") axis using the command plt-dot-ylabel. Finally, we can give our plot a title by using plt-dot-title. With all three functions, it's important to remember that the argument must be a string. If we forget to put our string in quotes, we'll get a syntax error. We can add these labeling functions in any order and at any place in our code as long as they come before plt-dot-show.

4. Legends

If we have multiple lines in the same plot, we'll want to add a legend. There are two steps for adding a legend: First, we must add the keyword argument label to each instance of plt-dot-plot. The label will be a string that we want in our legend. By adding a label to each plt-dot-plot function, we make our code easier to read. Now, if someone looks at this code, they know what each line is plotting before ever looking at the result. If we just type plt-dot-show now, we still won't see a legend. In order to add a legend, we must add a final function: plt-dot-legend. Like plt-dot-show, plt-dot-legend does not take any arguments. It just tells Matplotlib to use the labels from our plt-dot-plot functions to create a legend.

5. Arbitrary text

Sometimes, we just want to add a quick note directly onto our plot. We can do this by using the function plt-dot-text. This function takes three arguments: the x-coordinate where we want to put the text, the y-coordinate where we want to put the text, and the text we want to display as a string. Suppose we wanted to add a note to our ransom diagram? If we want the note to appear at the coordinated (5, 9), we can use this command, to produce this image.

6. Modifying text

Whether we're adding labels, legends, or arbitrary text, we might want to modify the letters in some way. There are two easy keyword arguments that we can add to any of our functions to change the way that text is displayed. If we want to change the size of font, we can use the keyword argument fontsize and pass in a float. If we want to change the color of font, we can use the keyword argument color and pass in a string. For a list of allowed color names, we can visit Wikipedia and look up "web colors".

7. Let's practice!

Great job! Let's return to the data from Officer Deshaun and his friends, and add some labels.


1. Adding some style

Previously, we learned how to create line plots and add labels, legends, and text. Now, we're going to make those plots beautiful.

2. And miles to go

After his success in graphing productivity, Officer Deshaun wants to plot the miles driven by each office over the course of a day. He's including more officers in this plot, so it's becoming more difficult to read. Officer Deshaun will improve is plot by customizing the the style of each line.

3. Changing line color

The first change Officer Deshaun might want to make to a line is to change its color. He can do this by adding the keyword argument "color" to the plt-dot-plot command. In this case, we plot six different lines in six different colors. The color keyword will accept a string corresponding to a "web color". For a list of allowed color names, visit Wikipedia and look up "web colors". In this example, we plot six different lines, each with a different color. You can't see the code, but we've added a legend so that you can see what each color appears on our plot.

4. Changing line width

Officer Deshaun can also modify the width of a line. The default line width is 1, but you can increase it using the keyword argument linewidth. In this example, Officer Deshaun plotted seven dataset, each with a slightly thicker line than the previous one. Again, we've hidden the extra code, but are showing a legend so that you can see what each linewidth looks like. Increasing line width can make graphs easier to use or can emphasize one line in a multi-line plot.

5. Changing line style

Officer Deshaun can further modify a line by changing its style. He can create different types of dashed lines using the keyword argument linestyle. Linestyle accepts several strings which correspond to different types of dashing. In this example, we plot four datasets with four different values of linestyle: a single hyphen for a normal line, two hyphens for a dashed line, a hyphen followed by a dot for a dot/dash line, and colon for a dotted line.

6. Adding markers

Adding markers is a great way to distinguish between different lines or to emphasize the location of data points. Officer Deshaun can add a marker using the keyword marker, which accepts several different strings. For example, if he sets marker equal to the letter s, the plot will have square markers. If he sets marker equal to the letter d, the plot will have diamond-shaped markers. In this example, we plot six different datasets, each with a different marker. Again, we hid the legend code but you can use it to see which symbols make each type of marker.

7. Setting a style

Sometimes, we want to make more drastic change to our graph. We can change the background, colors, and fonts for our entire graph by setting a style. The function plt-dot-style-dot-use accepts several different strings which correspond to different plotting styles. Here we show the same graph plotted in four different styles: fivethirtyeight (from the famous news site), ggplot (from another plotting library), seaborn (from yet another plotting library), and default (which is the normal style in matplotlib). Which do you think is best?

8. Let's practice!

You now know how to make many modifications to your line chart. Let's practice them.


## _Chapter 4_: Different Types of Plots

1. Making a scatter plot

Previously, we learned how to make line plots using Matplotlib. In this chapter, we'll learn about several additional plot types, starting with scatter plots.

2. Mapping Cell Phone Signals

We now know that Freddie Frequentist is the one who kidnapped Bayes, the Golden Retriever. Now we need to learn where he is hiding. Our friends at the police station have acquired cell phone data, which gives some of Freddie's locations over the past three weeks. The locations are given as a set of x and y coordinates on a map. In order to visualize these coordinates, we'll use a scatter plot.

3. What is a scatter plot?

A scatter plot allows us to show where each data point sits on a grid. Line plots let us visualize ordered data points, but scatter plots are a great way of viewing un-ordered points.

4. What is a scatter plot?

For example, suppose we want to learn about how Golden Retrievers grow during their first year of life. We might measure the heights of many puppies of different ages. We could then plot the height of each puppy versus its age.

5. Creating a scatter plot

In matplotlib, we create scatter plots by using the command plt-dot-scatter. The first argument is the x-data (in this example, this is the age of the puppy). The second argument is the y-data (in this example, this is the height of the puppy). We can then add labels and use plt-dot-show, just as we would for a line plot.

6. Keyword arguments

Many of the same keyword arguments that we used in line plots will work in scatter plots. In this example, we use the keyword argument "color" to change the marker color to green, and the keyword argument "marker" to change the marker shapes to squares.

7. Changing marker transparency

If we try to plot many points on the same scatter plot, things can get chaotic. We can't see if we've plotted many layers of points or just a single layer. We can make it easier to read by using a new keyword: alpha. Alpha accepts a number between 0 and 1 and makes each marker transparent. The smaller the alpha parameter, the more transparent the dot. Now we know that the darker areas have many points, while the lighter areas have fewer points.

8. Let's practice!

We've learned a new function: plt-dot-scatter. We can use it to plot two-dimensional data. We can use many of the old keyword arguments that we learned for plt-dot-plot along with a new argument: alpha, which controls transparency. Let's practice!


1. Making a bar chart

Previously, you learned how to make a line plot and a scatter plot using matplotlib. In this lesson, you will learn how to make a bar chart.

2. Comparing pet crimes

Officer Deshaun wants to examine the average number of pet kidnappings per month across different precincts. The best way to visualize a comparison of categorical data is by using a bar chart. Creating a bar chart in matplotlib is simple. We simply provide two arguments to the function plt-dot-bar: the labels for each bar, and the height of each bar. Because the x-axis is labeled for us, we only need to add a label for the y-axis using plt-dot-ylabel.

3. Horizontal bar charts

We can also make a horizontal bar chart. We simply use the function plt-dot-barh instead of plt-dot-bar. When we have many bars, it can be easier to fit all of the labels in with a horizontal bar plot.

4. Adding error bars

In the previous exercises, we've been plotting the average number of pets kidnapped per month. Averages don't always tell the full story. Often, we'll want to show some sort of error associate with our average, such as the standard deviation or standard error of the mean. We can add error bars to our bar chart by using the keyword argument yerr after our the first two positional arguments in plt-dot-bar. In this case, we are filling in yerr with a column of our DataFrame called "error".

5. Stacked bar charts

We know that Cityville had more pet kidnappings than either Farmburg or Suburbia. Office Deshaun wonders if this is true for both cat and dog kidnappings. The best way to compare both cat and dog kidnappings while also displaying the total number of kidnappings is to use a stacked bar chart. In a stacked bar chart, we display two different sets of bars. The height of each blue bar represents the number of dogs kidnapped. The height of each orange bar represents the number of cats kidnapped. The total height of the blue and orange bars represents the total number of pets kidnapped.

6. Stacked bar charts

To create this stacked bar chart, we start by plotting dogs column as we normally would. Here we show just one of the columns, which starts at y equals 0 and has a height of 4.

7. Stacked bar charts

Next, we stack the cat bars on top of the dog bars by using the keyword "bottom". Normally, a bar chart will start each bar at 0, but if we add the keyword "bottom" to plt-dot-bar and feed in the heights of our bottom bars (in this case df-dot-dog), matplotlib will plot the second set of bars starting where the first set of bars ends. For this particular bar, the cat bar starts at a height of y equals 4 and ends at a height of y equals 10. The height of the orange bar alone is 6.

8. Stacked bar charts

This is our complete set of code for creating a bar plot. Notice that we also added the keyword argument "label" for each bar plot to create our legend, just like we did for our line plots.

9. Let's practice!

You've just learned how to create a bar chart, add errors bars, and stack bar charts to make comparisons. Let's practice!


1. Making a histogram

Previously, we learned how to make line plots, scatter plots, and bar charts. In this lesson, we'll learn how to make one last type of plot: a histogram.

2. Tracking down the kidnapper

Freddie Frequentist left a dirty shoe print at the scene of the kidnapping. In that shoe print, there were many tiny pieces of gravel. Our crime lab was able to give us a Pandas DataFrame with the radius of each piece of gravel. We want to compare the distribution of gravel radii to samples from the three sites where Freddie could be hiding Bayes. If the distributions match, then the gravel in the footprint came from that site, and Freddie is likely hiding there.

3. What is a histogram?

The best way of visualizing the distributions of gravel radii is by creating a histogram. A histogram visualizes the distribution of values in a dataset. To create a histogram, we place each piece of data into a bin. In this example, we have divided our data into four bins: 1 - 10 mm, 11 - 20 mm, 21 - 30 mm, and 31 - 40 mm. The histogram tells us how many pieces of data (or pieces of gravel) fall into each of those bins. When we look at a histogram, we can quickly understand the entire dataset. Plotting a histogram in matplotlib is easy.

4. Histograms with matplotlib

We simply use the command plt-dot-hist. This function takes just one positional argument: our dataset. The output is shown on the right. By default, matplotlib will create a histogram with 10 bins of equal size spanning from the smallest sample to the largest sample in our dataset. If we want to change the number of bins,

5. Changing bins

we can use the keyword argument "bins". Bins accepts one integer. In this case, we divide our histogram into 40 bins. This allows us to see more detail in our histogram. If we want to zoom in on a specific piece of our dataset,

6. Changing range

we can use the keyword "range" to set the minimum and maximum value for our histogram. Note that we give the minimum and maximum values inside of a second set of parenthesis, separated by a comma. In this case, the minimum value is 50 and the maximum value is 100. Suppose we wanted to compare the distributions of weights

7. Normalizing

of male and female puppies. For some reason, we were able to collect many more samples of male puppy weights than female puppy weights. When we plot both histograms on the same axes, we can't actually see the difference in the distributions. In this case, we don't actually care about the absolute number of male puppies with a given weight. Instead, we care about what proportion of the dataset has that weight. We can solve this problem with normalization. Normalization reduces the height of each bar by a constant factor so that the sum of the areas of each bar adds to one. This would make our two histograms comparable, even if the sample sizes are different. We can normalize our histogram by using the keyword argument density equals True. Now each bar represents a proportion of the entire dataset. If a bar from the male puppies has the same height as a bar from the female puppies, both bars represent the same proportion of each population. Now that you've learned how

8. Let's practice!

to create histograms, let's practice by identifying where Freddy Frequentist is hidding!


1. Recap of the rescue

Congratulations! You found that Freddy Frequentist kidnapped Bayes the Golden Retriever and was hiding him at the Shady Groves Campsite.

2. You did it!

Bayes was safely returned to his DataCamp family. Now that you've solved the mystery of Bayes' kidnapping, let's recap what you've learned.

3. Modules and variables

First, you learned about modules. Modules help group together related tools in Python. You can use a module by importing it at the top of a script file, before writing any other code. You also learned about variables as you filled out Bayes' missing puppy report. Variables store data. Two common types of variables are strings (which represent text like Bayes' fur color) and floats (which represent numbers like Bayes' age).

4. Using functions

Next, you learned about functions. Functions are like machines that turn inputs into outputs. They can take positional arguments, which must come in a specific order, or keyword arguments, which come at the end and can go in any order. You used a license plate lookup function to get your first list of suspects.

5. Working with tabular data

You narrowed your list of suspects by using Pandas to examine credit card records. Pandas is a module that uses tools like DataFrames to examine and modify tabular data. You investigated the credit reports using functions like head and info, and selected specific rows that corresponded to suspicious purchases using logic.

6. Creating line plots

After narrowing the suspect pool down to two people, you collected handwriting samples and analyzed them using line plots. Matplotlib is a module for creating charts and visualizations. You used the plt-dot-plot function to create line plots and used keyword arguments to modify the color and linestyle of your plot. You used functions like plt-dot-legend and plt-dot-title to add annotations to your line plots. Line plots of the handwriting samples helped you learn that Freddy Frequentist was the kidnapper.

7. More plot types

You learned to create several other plot types using matplotlib. You used a scatter plot to investigate cell phone data and narrow Freddy's location to three possible campsites. You used bar charts to track how Officer Deshaun and his friends used their time to solve the mystery. Finally, you used a histogram to investigate the distribution of gravel radii on Freddy's shoeprint. This helped you determine that Bayes was being kept at the Shady Groves Campsite.

8. Great job!

You learned a lot while solving this mystery. Now that you're familiar with the basics of data analysis in Python, you can start learning other skills like machine learning or data visualization. Good luck! 