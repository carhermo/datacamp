# Data Types for Data Science in Python

## _Chapter 1_: Fundamental data types

1. Introduction and lists

Hello, I'm Jason Myers and welcome to the Python data types for data science course. I'm an author, open source maintainer, and an avid Python user.

2. Data types

In most programming languages, the data type system set the stage for the capabilities of the language. Understanding how to use the fundamental data types of a language greatly empowers you as a data scientist. You've already encountered integers and strings, so let's start with container sequences.

3. Container sequences

A container sequence gets its name because it holds a sequence of elements of other data types. In the data science world, we'll use these containers to store our data for aggregation, order, sorting, and more. Python provides several container sequences such as lists, sets, and tuples to name a few. They can be mutable meaning that they can have elements added and removed from them. Immutability, not able to be altered, allows us to protect our reference data, and replace individual data points with sums, averages, derivations, etc. We can iterate, also known as looping, over the data contained within these containers. Being able to iterate over these sequences allows us to group data, aggregate it, and process it over time. Let's start with learning about container types by looking at lists.

4. Lists

Often we need to hold an ordered collection of items, and lists allow us to do just that. Lists are mutable so we can add or remove data from them. Lists also allow us to access an individual element within them using an index. Let's see this in action.

5. Accessing single items in list

If I wanted to store a list of cookies I've eaten this week. I would begin by creating that list of cookies. When I eat another cookie, I'll want to add that to my list as well, which I can do with the append method. Then I can print the whole list of cookies. If I wanted to print the cookie I ate third; then I could use an index, which in this case would be 2 since indexes starts at zero, and print that element from the list. In addition to appending each cookie one at a time, we might also want to combine multiple lists.

6. Combining Lists

Python empowers us to combine lists in a few ways. First, we can use operators like the plus sign to add two lists together. When we do this, we will get a new list object returned to us. For example, we can add a list of cookies and cakes to create a list of deserts. Additionally, we can use the extend method on the list to combine two lists effectively appending all the values from a second list. You'll have the chance to use this in the exercises.

7. Finding Elements in a List

Earlier, we took advantage of the indexability of lists to return the item at a certain index. However, if we only know the value of an element, we can use it with the index method to get its index. Here I'm trying to remember when I had a sugar cookie.

8. Removing Elements in a List

Another reason I might want to know the index of a value is to remove it from a list with the pop method. Here I want to remove that sugar cookie from the list of cookies. So i pass the index I found earlier to the pop method, and I store the result of the pop method, which is the value we removed. Finally, I print the list so I can ensure it was removed.

9. Iterating over lists

Often when working with lists, we want to work on that list one element at a time. We can do that by iterating over the list using a for loop. Here I want to print each cookie I've eaten this week. So I loop over each cookie in the cookies list and print it.

10. Sorting lists

Python provides the sorted function that accepts an iterable such as a list and returns a new list with the elements in the proper order. Here I sort the cookies list and then print it so I can see the cookie names in alphabetical order. Time for you to try this on your own.

11. Let's practice!


1. Meet the Tuples

The next container type we want to learn about is the tuple. Tuples are widely used internally many of the systems we depend on like databases.

2. Tuple, Tuple

Tuples are very much like lists they hold data in the order, and we can access elements inside a tuple with an index. However, the similarities end there. Tuples are easier to process and more memory efficient than lists. Tuples are immutable, which means we can't add or remove elements from them. This is powerful because we can use them to ensure that our data is not altered. We can create tuples by pairing up elements. Finally, we can use something called unpacking to expand a tuple into named variables that represent each element in the tuple. Let's explore these features...

3. Zipping tuples

Often, we'll have lists where we want to matchup elements into pairs, and the zip function enables us to do that. Here I've got a list for the most popular cookies in the US and India, and I want to build a list of pairs by the popularity rank of the cookie in each country and I'll pass them the zip function. Then I can print the result of my zip and you can see I have what looks like a list of Tuples. It's really an iterator, but that's for a different course. Notice that the tuples use a parenthesis as their object representation.

4. Unpacking tuples

Now let's look at how we can use unpacking with a tuple. Tuple unpacking, also sometimes called tuple expansion, allows us to assign the elements of a tuple to named variables for later use. This syntax allows us to create more readable and less error prone code. Here I have a tuple containing the top ranked cookie from both countries, and I want to store them as us_num_1 and in_num_1. So that I can print them by name. I start by putting both variables as the target of the assignment statement separated by a comma. Then assign the first tuple in our top_pairs list to them. Let's look at a another great use case for tuple unpacking.

5. More unpacking in Loops

For me the place I use tuple unpacking the most is when working with loops. We can use tuple unpacking when declaring the for loop, to separate a list of tuples into their elements as we loop over them. This sounds a bit strange, but let's take a look at it. Here I'm building a for loop that uses tuple unpacking when iterating over the top_pairs list. It splits each tuple in the list into its Indian and US cookie elements. We then use each of these variables to print the cookies in order. Another use of tuple unpacking helps us keep track of which element in the iterable or list we are currently on.

6. Enumerating positions

Often we want to know what the index is of an element in the iterable is. The enumerate function enabled us to do that by creating tuples were the first element of the tuple is the index of the element in the original list, then the element itself. We can use this to track rankings in our data or skip elements we are not interested in. Here I'm going to enumerate our top pairs list and split that resulting tuple into idx and item. I can also use tuple unpacking on the item to get all three components separately. This can be exceptionally powerful. Let's look at a bit of responsibility that comes with this power.

7. Be careful when making tuples

When we are creating tuples, we can make them with zip or enumerate or use parentheses as shown here. However the real magic for creating a tuple in Python is the comma, if we accidentally end a line with a comma, we can create a tuple! This can have some very undesirable side affect further down in our code. Keep this in mind if you get a tuple where you don't expect it.

8. Let's practice!


1. Sets for unordered and unique data

Now that you've learned about lists and tuples let's look at our last built-in array data type the set. Sets are excellent for finding all the unique values in a column of your data, a list of elements, or even rows from a file.

2. Set

We use sets when we want to store unique data elements in an unordered fashion. For example, I might want to store a list of each type of cookie I had without any duplicates. Set are also mutable so I can add and remove elements from them. We're just going to scratch the surface of what can be done with a set. It has many more capabilities that align with set theory from math.

3. Creating Sets

A set is almost always created from a list. For example, I might have a list of cookies I've eaten today. I can make a set of them by passing them into the set constructor. Finally if I print it, you might notice that although I had three chocolate chip cookies in my list, once I make it a set, there is only one occurrence of it in the set. This occurs because sets only store unique items. Now let's explore modifying a set.

4. Modifying Sets

When working with a set we will use the add method to add a new element to the set. It will only add the element if it is unique otherwise it just continues on. Also, we can add multiple items using the update method. The update method takes a list of items and adds each one to the set if it is not present. While making the first two sections of this chapter, I ate two more cookies: a biscotti and a chocolate chip cookie. So let's use the add method to add those to our set, and print the result.

5. Updating Sets

Finally, Hugo also had some cookies, so let's use the update method to add the cookies he ate to our set and print them. Now let's learn how to remove some elements from our set.

6. Removing data from sets

When removing data from a set, we can use the discard method to safely remove an element from the set by its value. No error will be thrown if the value is not found. We can also use the pop method to remove and return an arbitrary element from the set. Let's remove the biscotti from our cookie set as Hugo and I had some debate about whether or not this was even a cookie with the discard method, and print the set. Next, we'll pop two cookies out of the list to decide what we could eat next. Next, we're going to leverage some of that set theory from math to perform very quick comparison operations.

7. Set Operations - Similarities

The union method on a set accepts a set as an argument and returns all the unique elements from both sets as a new one. The intersection method also accepts a set and returns the overlapping elements found in both sets. This is great for comparing data year over year for example. Speaking of examples, I'm going to create two new sets of the cookies Hugo and I have eaten. Then I'm going to use the union method to see the full set of cookies eaten by both of us. Finally, I use the intersection method to see the cookies that Hugo and I both ate. While these two methods help us find commonality, sets also provide methods to help us find differences.

8. Set Operations - Differences

We can use the difference method, which accepts a set, to find elements in one set that are not present in another set. The target we call the difference method on is important as that will be the basis for our differences. So here I want to see the cookies that I ate that Hugo didn't, which I can do by calling difference on my set of cookies and giving it Hugo's set and I can perform the reverse of this operation by using Hugo's list as the target.

9. Let's practice!


## _Chapter 2_: Dictionaries - the root of Python

1. Using dictionaries

Now that you are familiar with the container sequence types, let's dive into dictionaries. People often joke that everything in Python is a dictionary, and while it is a joke, there is a tiny bit of truth to that. I find myself using dictionaries the most of any container type. Dictionary are useful for storing key/value pair, grouping data by time or structuring hierarchical data like org charts. Let's take a look.

2. Creating and looping through dictionaries

Dictionaries hold data in key/value pairs. While the key must be alphanumeric, the value can be any other data type. It's also possible to nest dictionaries so that you can work with grouped or hierarchical data. We can also iterate over the keys and values of a dictionary. We can also iterate over the items of a dictionary, which are tuples of the key value pairs)! We create dictionaries using the dict() method or the braces shortcut. Here, I've got a list of tuples containing the name and zip for New York City Art Galleries. I'd like to turn that into a dictionary so I can quickly find the zip code without having to scan the whole list. I begin by creating an empty dictionary named art_galleries. Next I use tuple unpacking as I loop over the galleries list that contains my data. Then inside the loop I set the name of the gallery as the key in my dictionary and the zip code as the value. Finally, I'd like to find the last 5 art gallery names. By default when using sorted on a dictionary or looping over a dictionary we loop over the keys. So we'll print the keys which are names. Let's talk more about accessing dictionary values by key.

3. Printing in the loop

4. Safely finding by key

Here I'm looking for the Louvre gallery in New York City, but it's in Paris and throws an exception. Often you will know the key you want to get from the dictionary, and using the key as an index value to get the data. However, if the key is not present you will get an ugly exception that stops the execution of your code. While you could use Python exception handling, this is so common that dictionaries have a get() method just for this problem.

5. Safely finding by key (cont.)

The get method allows you to safely get a value from a key by passing the key. If the key is not found in the dictionary, it returns None. You can optionally supply a second argument which will be returned instead of None. Continuing from the prior example of looking for the Louvre, I make the same request with the get method, and supply the string Not Found to be returned if it is not present, which is exactly what happens. Finally, I supplied a valid key to the get() method and it returned the value from the dictionary. Accessing data in a safe manner is critical to ensure your programs execute properly. Let's learn more about nested data.

6. Working with nested dictionaries

I mentioned earlier that you can also nest dictionaries to group data or establish a hierarchy. I've reorganized my prior art galleries dictionary to by keyed by zip code and then gallery name with the value of their phone number. I can use the keys() method to see the list of zip code keys. I can print the value of one of those zip codes so you can see the dictionary nested under it,

7. Accessing nested data

and I can get the phone number of one of those galleries by using the gallery name which is the nested dictionary's key as a secondary index. Nesting dictionaries is a very common way to deal with repeating data structures such as yearly data, grouped, or hierarchical data such as organization reporting structures. You access nested values by providing multiple indices to the dictionary or using the get method on an index.

8. Let's practice!

It's your turn to practice. 


1. Altering dictionaries

In the prior video we learned that dictionaries are mutable, so we can alter them in a number of ways. Let's start by adding data to them. Which is something that you, as a data scientist, will need to do all the time.

2. Adding and extending dictionaries

You can add data to a dictionary just by using a new key as an index and assigning it a value. It's also possible to supply a dictionary, list of tuples or a set of keywords arguments to the update() method to add values into a dictionary. I have a dictionary that contains the art galleries in the 10007 zip code and I want to add it to my art_galleries dictionary. I assign the zip code as the key and then the dictionary as the value.

3. Updating a dictionary

I can also create a list of tuples, and supply them to the update() method. Here I'm supplying them to a dictionary index of the zip code since I want this data to be nested underneath it. Finally I'll print the zip code to be sure they were added. Notice how those are now a dictionary under that key. Often, We'll also want to narrow down our dataset to only the data which is relevant to the problem at hand, so let's learn how to remove data.

4. Popping and deleting from dictionaries

You can use the del python instruction on a dictionary key to remove data from a dictionary. However, it's important to remember that del will throw a KeyError if the key you are trying to delete does not exist. The pop() method provides a safe way to remove keys from a dictionary. Let's start by removing all the galleries from zipcode '11234', then I'm not sure if there are any galleries in zip code 10310, so I'm going to pop that zip code from the dictionary and save it. Finally, I'll print the popped value.

5. Let's practice!


1. Pythonically using dictionaries

So far, we've been working with dictionaries in a straight forward manner, but Python has more efficient ways to work with them. We refer to these manners of interacting as being Pythonic.

2. Working with dictionaries more pythonically

Previously, we looped through dictionary keys then used the keys to get the values we desired. Python provides an items() method which returns a dict_items object, but we can iterate over it as a list of key/value tuples. If I use the dot items() method on my original art_galleries dictionary it returns a tuple of the gallery name and the phone number. I can use tuple unpacking to go ahead and expand that into its two components in the loop. Finally, I print them so you can see the information. Now let's look at a more Pythonic method for checking if data is present in a dictionary.

3. Checking dictionaries for data

Earlier, we used the get method to safely look for keys, and we can use that to check to see if a key is in a dictionary. However, Python provides the in operator to see if a key is in a dictionary. It returns a boolean (True or False). Here I'm checking to see if 11234 is in the art_galleries dictionary, and since I deleted that key earlier it returns false. Since it returns a boolean, it is often used in conditionals statements like an if/else statement. Here if the key 10010 is in the art_galleries dictionary I'm going to print a message saying that it was found and with the value of the key.

4. Let's practice!


1. Working with CSV files

Okay, we're going to take a break from all this talk of types and look at where our data has been coming from so far: CSVs or Comma Separated Value files.

2. CSV Files

Many of the data sets you're going to encounter as a data scientist will be published as CSV files, because it's one of the most common ways to dump data from a database or storage system. Here is an example of a CSV file for the Art galleries we've been working with. You can see it begins with a header line that identifies the fields in each row. Then a line for each row of data. CSV files are not required to have a header. Let's look at how to read data from these.

3. Reading from a file using CSV reader

Python provides a wonderful module called csv to work with these types of files. To create a Python file object, you use the open() function, which accepts a file name and a mode. The mode is typically 'r' for read or 'w' for write. Then we pass the file object to the dot reader() method on the csv module and use it as you would any other iterable. Here I import the csv module, create a file object for the ART_GALLERY dot csv data file. Then I use the csv dot reader to read the file and iterate over each line or row in the file and print it.

4. Reading from a CSV - Results

Notice this files first line is a header record which contains the fields found in each record of the file.

5. Creating a dictionary from a file

The Python CSV module also provides a way to directly create a dictionary from a csv file with the DictReader class. If the file has a header row, that row will automatically be used as the keys for the dictionary; however, if not you can supply a list of keys to be used. Each row from the file is returned as an ordered dictionary. We'll learn more about OrderedDicts in a later video. Let's review an example of how this works. It's all the same until line there where we use the DictReader instead of the reader method. Then we print the row, we get an OrderedDict that looks like a list of tuples; however, it operates just like a normal dictionary. Okay time for you to practice.

6. Let's practice!


## _Chapter 3_: Meet the collections module

1. Counting made easy

As a data scientist, we're often going to need to count items, create dictionaries values before we know keys to store them in, or maintain order in a dictionary.

2. Collections Module

The collections module is part of Python standard library and holds several more advanced data containers which solve these problems and more. Let's start our tour of the collection module by learning about Counter.

3. Counter

Counter is a powerful python object based on the dictionary object that accepts a list and counts the number of times a value is found within the elements of that list. Since it's based on a dictionary, you can use all the normal dictionary features. Here, I have an list named nyc_eatery_types that contains one column of data called type from a table about eateries in NYC parks. I create a new Counter based on that list and print it. You can see each type from list and the number of times it was found in the list. I can also see how many restaurants are in the counter by using Restaurant as the index and printing it. Counters also provide a wonderful way to find the high values they contain.

4. Counter to find the most common

The most_common() method on a Counter returns a list of tuples containing the items and their count in descending order. Here I'm printing the top 3 eatery types in the NYC park system with the most_common method and passing it 3 as the number items to return. most_common() is great for frequency analytics, how often something occurs, a problem I encounter often when working on data science problems. Now it's your turn.

5. Let's practice!


1. Dictionaries of unknown structure - defaultdict

Often, we'll be working with data were we don't know all the keys that will be used, but we want to store a complex structure under those keys. A good example is I want every key to have a list of values. I'd have to initialize every key with an empty list then add the values to the list. Here is an example of just that.

2. Dictionary Handling

You can I start by looping over a list of tuples with the park's id and the name of the eatery. Then I check to see if I have a list for that part already in my dictionary. If not I create an empty list. Next, I append the name of the eatery to the list for that park id. Thankfully, collections provides an easier way using defaultdict.

3. Using defaultdict

Defaultdict accepts a type that every value will default to if the key is not present in the dictionary. You can override that type by setting the key manually to a value of different type. Still working with the NYC Parks eatery data, I have a list of tuples that contain the park id and the name of an eatery. I want to create a list of eateries by park. I begin by importing defaultdict from collections. Next, I create a defaultdict that defaults to a list. Next I iterate over my data and unpack it into the park_id and name. I append each eatery name into list for each park id. Finally, I print the eateries in the park with the ID os M010, which if you are curious is Central Park. Let's look at another example.

4. defaultdict (cont.)

It's also common to use a defaultdict as a type of counter for a list if dictionaries where we are counting multiple keys from those dictionaries. In our NYC park eateries, I was curious how many had a published phone number or a website. This time when creating my defaultdict, I tell it I want it to be an int. Then I look over my nyc_eateries data and add 1 to the phones key on my defaultdict if it has a phone number that is not None. Then I add 1 to the websites key if it has a website. Finally, I print my defaultdict to see the counts. Let's go practice.

5. Let's practice!


1. Maintaining Dictionary Order with OrderedDict

Often we want to store data in the dictionary in an ordered fashion. For example, I might want to store the data in order by date, or by a ranking.

2. Order in Python dictionaries

Normal dictionaries don't maintain order of the keys that you insert into them in versions of Python below 3-point-6. Recently in Python 3-point-6 they started storing dictionary order. However, the collections module provides an OrderedDict that maintains the order that keys and values as they were added to the dictionary without regard for the Python version. You might remember this from our video using the CSV dictreader which returned each row as an OrderedDict. Let's take a look at an example using an OrderedDict.

3. Getting started with OrderedDict

We begin by importing OrderedDict from collections. Next, I create an OrderedDict to hold the NYC Park data by permit end date. Then I loop over the NYC park eateries which are ordered by permit end date already and add them to the OrderedDict by that date. Finally, I can print the first three permit end dates by making a list of the items and slicing it.

4. OrderedDict power feature

Using the OrderedDict from the previous example, I use popitem() and get the permit that expires the latest, a second popitem() returns the next latest expiration. Finally, I can pop the lowest end date in the dictionary by using popitem last equals False here. Your turn to experiment with an OrderedDict. Let's do some work with OrderedDicts.

5. OrderedDict power feature (2)

6. Let's practice!


1. namedtuple

Often times when working with data, you will use a dictionary just so you can use key names to make reading the code and accessing the data easier to understand.

2. What is a namedtuple?

Python has another container called a namedtuple which is a tuple, but has names for each position of the tuple. This works well when you don't need the nested structure of a dictionary or desire each item to look identical, and don't want to add the overhead of a Pandas dataframe.

3. Creating a namedtuple

You create a namedtuple by passing a name for the tuple type and a list of field names. Let's begin by importing namedtuple from the collections module. Next we'll define our namedtuple. It's common practice to use Pascalcase (capitalizing each word) when naming namedtuples so hear I've used Eatery with a capital E for both the tuple name and the variable we stored the namedtuple as. Then we provide a list of fields we want on the nametuple. Now we can create our Eatery named tuples. I'm going to change our NYC Park eateries data into a list of namedtuples. I create an empty list then iterate over my nyc_eateries list creating an instance of my Eatery namedtuple by passing in the data from the loop as the arguments to my namedtuple.

4. Print the first element

Finally, let's print the first Eatery in the list. Now that we've got a list of named tuples let's see how we can use them.

5. Leveraging namedtuples

One of the great things about named tuples is that they can make code clearer because each field is available an as attribute. An attribute is basically a named field or data storage location. We can also depend on every instance of a namedtuple to have all the fields, although some might be empty or None in Python terms. This means we can always have safe access to a field without the need for a get method like a dictionary. Here I'm going to use the list of tuples we created in the prior slide, and print the name, park_id, and location for the first three entries in the list. Now it's your turn to practice.

6. Let's practice!


## _Chapter 4_: Handling Dates and Times

1. There and Back Again a DateTime Journey

Dealing with dates and times is often considered to be very confusing with all the considerations due to the unique ways in which time flows. Leap Years, Different length months, different distribution of weekdays/weekends, and the dreaded timezones are just a few of the things we must consider. However, with careful reasoning, you'll soon be working with datetime data with relative ease. Only practice and experience can make you fluent in datetime issues, so let's start on this learning journey. I'll be using data from the NYC parking violations dataset.

2. From string to datetime

When working with datetimes in Python, we use the datetime module from the standard library. There is a datetime type inside of the datetime module. In addition to letting us manually create datetime objects, we can also parse existing strings into datetime objects. This is an exceptionally common task. So let's dive into an example. We being by importing the datetime type from the datetime module. Next, I'm printing the string that we are going to parse to create a datetime object.

3. Parsing strings into datetimes

Next I use the strptime method to parse the parking_violations_date using a format string for MM/DD/YYYY (more on this in just one sec). Finally, I print the datetime object so we can see the results. Okay, so about those format strings...

4. Time Format Strings

Time format strings are common across many different programming language, and originated in C. Here are the few that we just used with the strptime method. You use the directives and any special characters or spaces you need to match the datestring you are trying to parse. There is a complete list of them in the Python Documentation, and I linked in the exercises for you to reference. In addition to using these format strings for converting from a string to a datetime,

5. Datetime to String

we can also use them to go from a datetime to a string object with the strftime() method. For example, we can use the same format string to output the datetime we created on the prior slide. We can also output the string as an ISO standard datetime string, one of the most common ways to express times when dealing with writing to or reading from files or applications. Let's use it on our datetime object, we've been working with. Time for you to practice.

6. Let's practice!


1. Working with Datetime Components and current time

Once we have a datetime object, we can work with it to get parts of the datetime like the month, year, or day. We can also get the current time and manipulate a timezone. Let's begin by working with the parts of a datetime object.

2. Datetime Components

All the parts of a datetime object are available as attributes, such as day, month, year, hour, minute, second etc. These are often used to group data by a particular time frame. Let's count the NYC parking violations for 2016 and group by day. We'll start by using a defaultdict of ints to count the records by day. Next, I'll loop over the packing violations to and parse the date which is found in the fifth element of our list. Next I increment the appropriate day based on the violation.

3. Datetime Components - Results

Finally I'm going to sort the days of the month and print the day of the month and our violation count for that day.

4. What is the deal with now

Often when working with datetime objects, you'll want to work on windows or ranges that start from the current date and time. We can do this using the datetime now functions. There is a dot now() method on the datetime object in the datetime module and a dot utcnow() method. The dot now() method returns the current local time on the machine on which it is run, and dot utcnow() does the same thing but returns the value in UTC timezone. We'll talk more about timezones in a second. Let's start by importing datetime type. Then we'll call the now method on it and it will return a new datetime representing the current time. Let's print it to see what we got.

5. What is the deal with utcnow

Next, let's call utcnow and get the current time in the UTC timezone. Finally, print it. The UTC timezone is only timezone with this special kind of method. Let's learn a bit more about timezones.

6. Timezones

Timezones can make life very interesting! By default, any datetime you make using the now methods are "naive" datetime objects, which means they are missing their timezone that is required to make an "aware" datetime object. You'll often get data where timezone is not supplied and you need to set it manually. In order to work effectively with other timezones, you can use the pytz module and use the timezone names from the Olsen database, the standard for timezone information. An "aware" datetime object has an dot astimezone() method that accepts a timezone object and returns a new datetime object in the desired timezone. If the tzinfo is not set for the datetime object it assumes the timezone of the computer you are working on. Let's see this in action.

7. Timezones in action

I'll begin by importing timezone from pytz. I've got the datetime of a violation, and parsed it into a naive datetime object. Next, I'm going to prepare the timezone objects I'm going to work with. Then, I create an object to present the Eastern timezone that NYC is in, and then the timezone for LA. Next, I use the replace method to replace the empty timezone on my record datetime and save it as my datetime. Now that I have an aware datetime instance, I can use the as timezone method to get the records time in LA.

8. Timezones in action - results

Finally, I print them both. This type of conversion is great for ensuring that data remains relevant to the viewer by presenting it in their native timezone. Your turn to work with these concepts.

9. Let's practice!


1. Time Travel (Adding and Subtracting Time)

Another common thing to do with time object is to peer into the future or past to find data. Let's look at how that works.

2. Incrementing through time

A very common case when working with times is to get a date 30, 60, 90 days in the past from some date. In Python we use the timedelta object from the datetime module to represent differences in datetime objects. You can create a timedelta by passing any number of keyword arguments such as days, seconds, microseconds, milliseconds, minutes, hours, and weeks to it. Once we have a timedelta object, we can add or subtract it from a datetime object to get a datetime object relative to the original datetime object. Let's look at how this works in practice. First, we import timedelta from the datetime module. Next we'll make a timedelta instance called flashback for 90 days. Then, we can see the starting point by printing the datetime we'll be working with.

3. Adding and subtracting timedeltas

Then I'll print the the date minus 90 days and finally plus 90 days. This can be useful to compare a date this year to one from the prior year, compare by quarter as we did here, or compare month to month. Let's look at how we can find the time between two dates.

4. Datetime differences

Just like we were able to subtract a timedelta from a datetime to find a date in the past, we can also calculate the difference between two dates to get the timedelta between in return. Let's find out how much time has elapsed between the first two violations in our list. I've already parsed both dates into record_dt and record2_dt, respectively. Now I subtract one from the other and store the result as time_diff. This returns a timedelta, which I used the type function to demonstrate here, that represents the time difference between the two datetimes. Finally, I can print the time_diff and see that that time difference between violations was 4 seconds! In fact after some more digging, I was able to determine that the average difference was 15-point-3 seconds! That's a lot of parking violations! Now, it's your turn to practice this.

5. Let's practice!


1. HELP! Libraries to make it easier

There are several third-party libraries that make parsing, converting, and working with dates and times easier. One of most popular is the Pendulum library. Let's use Pendulum here to do some common datetime operations.

2. Parsing time with pendulum

Pendulum provides a powerful way to convert strings to pendulum datetime objects via the dot parse() method. Just pass it a date string and it will attempt to convert into a valid pendulum datetime. Here is a great example of how helpful parse can be. In the raw data for the parking violations we have been working with this chapter, it contains the date and time in separate columns, and the AM/PM indicator is just a single character. So I start by importing pendulum. Using the first parking violation, I'm building a string called occurred that is the date plus a space plus the time and appending an M to complete the AM/PM indicator. Then, I use pendulum dot parse() on the string and instruct pendulum that it's in the Eastern timezome all on one line! Finally, I print the date that pendulum parsed. (Note that they use a aware version os the ISO standard string). If we were to do this with datetime, we'd need to use strptime with a format string, and use the replace method to fix the timezone. Let's look at another place where pendulum is helpful - timezones.

3. Timezone hopping with pendulum

Pendulum has wonderful support for timezones, and comes with the Olsen Database that I mentioned earlier built into it. It provides an in_timezone() method that can be used to convert a pendulum object to a desired timezone. Also, Pendulum's now method accepts a timezone so you can generate the current time easily for any location in the world. I saved the results for our pendulum parsing example into the violations_dts list as you can see here.

4. More timezone hopping

I'm going to iterate over the records and convert them all the to Tokyo timezone. Finally, I'm going to use the now() method with the Tokyo timezone to get the current time in Tokyo.

5. Humanizing differences

Pendulum has an alternative to timedelta called a period when calculating the difference between two dates by subtraction that provides methods such as dot in_days/weeks/hour/minutes and in_words to output in a chosen manner. Let's take two of our violation datetimes and calculate the difference. Here you can see the period object where we got a timedelta previously. Next I use the in_words() method to get a nice English representation of the difference.

6. More human than human

Pendulum provides the ability to set a locale and get it in other languages. Then, I use the in_days() method to see the difference in days and finally I use in_hours to see the difference in hours. This is just a taste of what pendulum can do. It would be well worth your time after this class to play more with pendulum and incorporate it into your work. Your turn to take it for a spin.

7. Let's practice!


## _Chapter 5_: Answering Data Science Questions

1. Case Study - Counting Crimes

Now it's time to put your learning to practical use with a case study on the Chicago Crime Data.

2. Data Set Overview

Our data is in a CSV file and looks like you see here. It contains the details for each crime in the City of Chicago. It's worth noting that I've shrunk the dataset down to be more manageable via sampling so these don't represent every crime that occurred. The full dataset is available on the City of Chicago's Open Data Portal.

3. Part 1 - Step 1

You'll begin this case study by reading data from a CSV and building a list to hold your data.

4. Part 1 - Step 2

In the second step, you'll count the data by month using a counter similarly to how we did previously, but with a small twist, which I'll come back to in a second. Then you'll use the month date part to group/count the data by month. So about that twist. In the date part grouping example we used a defaultdict. Here you'll be using a counter. Hint: both work the same way since both are based on dictionaries.

5. Part 1 - Step 3

Next, we want to extract the data into a dictionary keys by month that stores a list of the location types where the crimes occurred that month. We'll use the defaultdict we learned about in when working on eateries coupled with the date component grouping we just used in the prior step and learned earlier.

6. Part 1 - Final

So to answer the real question, "What are the five most common crime locations per month?" We'll use a Counter on our new dictionary as we did previous to find the answer we're seeking. Good luck with part 1!

7. Let's practice!


1. Case Study - Crimes by District and Differences by Block

Now let's try our hand at the second part of the case study. First, We're going to determine how many crimes occurred by district, and then look at how types of crimes differ between city blocks.

2. Part 2 - Step 1

Previously, we read the data directly from the csv file into a dictionary as shown here. This example shows how to use the dictreader which gives you a dictionary per row of the file, you'll need to determine how to properly build the dictionary based on the assignment instructions. Then we can use that dictionary to do things like pop out a key, as we did in a prior exercise, and store the remainder of the data under that key in another dictionary. Here is a reminder of how to pop data from a dictionary, which leaves the original dictionary with everything but that key and value still in place.

3. Part 2 - Step 2

Then we want to determine how many crimes occurred by district. You'll need to loop over the dictionary Pythonically and use Counter and defaultdict as we have several times in this case study. Here is an example of how we Pythonically looped over dictionaries in our videos.

4. Wrapping Up

In the last step of our case study, we've identified a few blocks of data we want to concentrate on and see the differences in crimes that occur in these locations. First you'll want to take a list and get a unique set of crimes for that block as we did in chapter one. Then you'll look for difference in the unique crime sets using the set difference method as we did at the end of chapter 1. This will complete the case study! Good luck!

5. Let's practice!


1. Final thoughts

Congratulations, you made it! You've learned the fundamentals of data types and how to use them in many different ways with Python. You are able to bend lists, sets, and dictionaries to your will and use them to answer data science questions. You explored the collections module to maintain order, establish defaults, and count furiously. You have traveled through time with datetime objects and the Pendulum library. Then you put it all together in a case study. Again, congratulations on completing the course and thank you!

2. Congratulations